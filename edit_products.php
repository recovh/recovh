<?php
  include("assets/php/mongodb_functions.php");
  if(isset($_GET['pid'])){
    $pid = $_GET['pid'];
    $prd = show_single_product($pid);

  }else{
    header("location: index.php");
  }
?>
<html>
<head>
<title>MongoDB Project</title>

<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<header>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">MongoDB</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01" style="margin-left:100px;">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Features</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Pricing</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">About</a>
      </li>
    </ul>
    <ul class="navbar-nav my-2 my-lg-0">
      <li class="nav-item">
        <a class="nav-link" href="login">Login</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="register">Register</a>
      </li>
    </ul>
    
  </div>
</nav>
</header>

<div class="container-fluid">
  <h1 style="text-align:center;margin-top:50px;"> Sample MongoDB Database Application</h1> 

  <div class="row">
    
    <div class="col-md-6">
      <form id="prd_form" method="post" action="assets/php/mongodb_functions.php" style="width:60%;margin:80px auto;">
      <fieldset>
        <legend>Edit Products</legend>
        <small id="form-text" class="form-text text-muted">
         
        </small>
      
        <div class="form-group">
          <label for="prd_name">Name</label>
          <input type="text" class="form-control" required name="prd_name" id="prd_name" value="<?php echo $prd->name ?>" placeholder="Enter Product Name">
          
        </div>
        
        <div class="form-group">
          <label for="prd_email">Price in $</label>
          <input type="text" class="form-control" required name="prd_price" id="prd_price" value="<?php echo $prd->price ?>" placeholder="Enter Price">
          
        </div>

        <div class="form-group">
          <label for="prd_phone">Description</label>
          <input type="text" class="form-control" required name="prd_description" id="prd_description" placeholder="Enter Description" value="<?php echo $prd->description ?>">
          
        </div>
        <input type="hidden" name="prd_id" value="<?php echo $prd->name ?>"/>
        <button type="submit" name="prd_update" id="prd_submit" class="btn btn-primary">Update</button>
        <button type="button" class="btn btn-danger" onclick="window.location.href='index.php'">Cancel</button>
      </fieldset>
    </form>
  </div>
  
  </div>
  </div>
</div>
</body>
</html>