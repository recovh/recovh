<?php
	$mng = new MongoDB\Driver\Manager("mongodb://localhost:27017");
	$bulk = new MongoDB\Driver\BulkWrite;
    $db_name = "php_mongodb.products";

	if(isset($_POST['prd_submit'])){
		$prd_name = $_POST['prd_name'];
		$prd_price = $_POST['prd_price'];
		$prd_description = $_POST['prd_description'];
		
	    $doc = ['name' => $prd_name, 'price' => $prd_price,  'description' => $prd_description];
	    $bulk->insert($doc);
	    $mng->executeBulkWrite($db_name, $bulk);

	    echo "<script>
		alert('Product added successfully');
		window.location.href='../../index.php';
		</script>";
		exit;
	}
	if(isset($_POST['prd_update'])){
		$prd_name = $_POST['prd_name'];
		$prd_price = $_POST['prd_price'];
		$prd_description = $_POST['prd_description'];
		$pid = $_POST['prd_id'];
		
	    $bulk->update(['name' => $pid], ['$set' => ['name' => $prd_name]]);
    	$bulk->update(['name' => $pid], ['$set' => ['price' => $prd_price]]);
    	$bulk->update(['name' => $pid], ['$set' => ['description' => $prd_description]]);
    	
    	$mng->executeBulkWrite($db_name, $bulk);

	    echo "<script>
		alert('Product updated successfully');
		window.location.href='../../index.php';
		</script>";
		exit;
	}

	if(isset($_GET['str']) && ($_GET['str'] == "prd_delete")){
		$pid = $_GET['prid'];
		
	    $bulk->delete(['name' => $pid]);
    
    	$mng->executeBulkWrite($db_name, $bulk);

	    echo "<script>
		alert('Product deleted successfully');
		window.location.href='../../index.php';
		</script>";
		exit;
	}
	function show_products(){
		$mng = new MongoDB\Driver\Manager("mongodb://localhost:27017");
			
		$query = new MongoDB\Driver\Query([]); 
     	$db_name = "php_mongodb.products";

	    $rows = $mng->executeQuery($db_name, $query);
	    
	    return $rows;
	    
	}

	function show_single_product($pid){
		$mng = new MongoDB\Driver\Manager("mongodb://localhost:27017");
		//$pid = ObjectId($pid);
		$filter = [ 'name' =>  $pid ]; 
	    $query = new MongoDB\Driver\Query($filter);     
	    $db_name = "php_mongodb.products";
	
	    $res = $mng->executeQuery($db_name, $query);
	    
	    $row = current($res->toArray());

	    return $row;
	}
?>